package pl.sda.zadania.obiektowosc.zad2_3;

public class MojaData {
    int dzien;
    int miesiac;
    int rok;

    public MojaData(int dzien, int miesiac, int rok) {
        this.dzien = dzien;
        this.miesiac = miesiac;
        this.rok = rok;
    }

    //a
    public void wyswietlDate(){
        System.out.printf("%d.%d.%d\n",dzien,miesiac,rok);
    }
    //b
    public void wyswietlDate2(){
        System.out.printf((dzien<10?"0":"")+dzien+".%d.%d\n",miesiac,rok);
    }

    //c
    public void wyswietlDate3(){
        String[] miesiace = new String[]{"styczeń","luty","marzec","kwiecień","maj","czerwiec","lipiec","sierpień",
        "wrzesień","październik","listopad","grudzień"};
        System.out.printf("%d %s %d\n",dzien,miesiace[miesiac-1],rok);
    }

}
