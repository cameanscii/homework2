package pl.sda.zadania.obiektowosc.zad2_3;

public class Wydarzenie {
    public String nazwaWydarzenia;
    public MojaData dataWydarzenia;
    MojaData dzis = new MojaData(25,06,2018);

    public Wydarzenie(String nazwaWydarzenia, MojaData dataWydarzenia) {
        this.nazwaWydarzenia = nazwaWydarzenia;
        this.dataWydarzenia = dataWydarzenia;
    }

    public int ilePozostaloLAt(){
        return dataWydarzenia.rok - dzis.rok;
    }
    public int ilePozostaloMiesiecy(){
        return dataWydarzenia.miesiac - dzis.rok;
    }
    public int ilePozostaloDzien(){
        return dataWydarzenia.dzien - dzis.dzien;
    }
}
