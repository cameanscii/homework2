package pl.sda.zadania.obiektowosc.zad1;

public class SzybkiSamochod extends Samochod{

    int predkosc=20;

    public SzybkiSamochod(String kolor, String marka, int rocznik) {
        super(kolor, marka, rocznik);
    }

    public void przyspiesz() {

        if (predkosc >= 200) {
            System.out.println("Samochód nie może jechać więcej niż 200km/h");

        } else {
            predkosc += 20;
            System.out.printf("Przyspieszam do %d km/h\n", predkosc);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SzybkiSamochod) {
            Samochod ten = (SzybkiSamochod) obj;
            if (this.kolor == ten.kolor && this.marka == ten.marka && this.rocznik == ten.rocznik&&this.predkosc==ten.predkosc) {
                return true;
            }
        }
        return false;
    }
}
