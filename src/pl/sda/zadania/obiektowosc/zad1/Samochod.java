package pl.sda.zadania.obiektowosc.zad1;

public class Samochod {
    int predkosc = 0;
    protected String kolor;
    protected String marka;
    protected int rocznik;

    public Samochod(String kolor, String marka, int rocznik) {
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    public void przyspiesz() {

        if (predkosc >= 140) {
            System.out.println("Samochód nie może jechać więcej niż 140km/h");

        } else {
            predkosc += 20;
            System.out.printf("Przyspieszam do %d km/h\n", predkosc);
        }
    }


    @Override
    public String toString() {
        String stringer = String.format("%s %s rocznik %d", kolor, marka, rocznik);
        return stringer;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Samochod) {
            Samochod ten = (Samochod) obj;
            if (this.kolor == ten.kolor && this.marka == ten.marka && this.rocznik == ten.rocznik) {
                return true;
            }
        }
        return false;
    }
}
