package pl.sda.zadania.obiektowosc.zad10;

public class Pracownik {
    String imie;
    String nazwisko;
    int miesieczneWynagrodzenie;

    public Pracownik(String imie, String nazwisko, int miesieczneWynagrodzenie) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.miesieczneWynagrodzenie = miesieczneWynagrodzenie;
    }
}
