package pl.sda.zadania.obiektowosc.zad10;

public class Firma {
    static Pracownik[] pracownicy;

    Firma(Pracownik[] pracownicy) {
        this.pracownicy = pracownicy;
    }

    int obliczMiesiecznyKosztFirmy(){
        int kosztMiesieczny=0;
        for(Pracownik x:pracownicy){
            kosztMiesieczny+=x.miesieczneWynagrodzenie;
        }
        return kosztMiesieczny;
    }
     int obliczRocznyKosztFirmy(){
        int kosztRoczny=12*obliczMiesiecznyKosztFirmy();
        return kosztRoczny;
    }

    int obliczKosztFirmy(Czas czas, int iloscCzasu){
        int koszt=0;
        switch (czas){
            case DZIEŃ:
                koszt = iloscCzasu*(obliczMiesiecznyKosztFirmy()/30);
                break;
            case MIESIĄC:
                koszt = iloscCzasu*obliczMiesiecznyKosztFirmy();
                break;
            case ROK:
                koszt=iloscCzasu*obliczRocznyKosztFirmy();
                break;
        }
        return koszt;

    }


}
