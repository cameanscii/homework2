package pl.sda.zadania.obiektowosc.zad10;

public class Main {
    public static void main(String[] args) {
        Pracownik marek = new Pracownik("Marek","Kamiński", 1000);
        Pracownik martyna = new Pracownik("Martyna", "Wojciechowska", 2000 );
        Pracownik kaziemierz = new Pracownik("Kazimierz", "Nowak", 12000);
        Pracownik[] zespol = new Pracownik[]{marek,martyna,kaziemierz};
        Firma podrozeDoOkolaSwiata = new Firma(zespol);

        System.out.println("Miesięczny koszt firmy wynosi: "+ podrozeDoOkolaSwiata.obliczMiesiecznyKosztFirmy()+" zł");
        System.out.println("Roczny koszt firmy wynosi: "+ podrozeDoOkolaSwiata.obliczRocznyKosztFirmy()+" zł");

        Czas zlecenie = Czas.MIESIĄC;
        int iloscCzasuZlecenia = 10;
        int kosztZlecenia = podrozeDoOkolaSwiata.obliczKosztFirmy(zlecenie,iloscCzasuZlecenia);
        System.out.println("Koszt Firmy na okres czasu "+4+" "+zlecenie.nazwa+" wyniósł "+kosztZlecenia+" zł");


    }
}
