package pl.sda.zadania.typyWyliczeniowe.zad2;

public enum Kolor {
    KIER("♥"),
    PIK("♠"),
    KARO("♦"),
    TREFL("♣");




    String symbol;


    Kolor(String s) {
        this.symbol=s;
    }
    public String getSymbol(){
        return symbol;
    }
}
