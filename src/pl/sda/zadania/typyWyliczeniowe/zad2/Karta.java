package pl.sda.zadania.typyWyliczeniowe.zad2;

public class Karta {
    Kolor kolor;
    Figura figura;

    public Karta(Kolor kolor, Figura figura) {
        this.kolor = kolor;
        this.figura = figura;
    }

    @Override
    public String toString() {
        String nazwa = figura.toString().substring(0, 1) + figura.toString().substring(1).toLowerCase();
        String znak = kolor.getSymbol();
        return nazwa + " " + znak;
    }

    public static void main(String[] args) {


        Kolor[] kolorTab = Kolor.values();
        Figura[] figuraTab = Figura.values();
        Karta[] kartaTab = new Karta[kolorTab.length * figuraTab.length];
//        kartaTab[0] = new Karta(kolorTab[0], figuraTab[0]);
//        System.out.println(kartaTab[0].toString());

        int licznik = 0;
        for (Kolor kolor1 : kolorTab) {

            for (Figura figura1 : figuraTab) {
                kartaTab[licznik] = new Karta(kolor1, figura1);
                System.out.println(kartaTab[licznik].toString());
                licznik++;
            }

        }
    }
}

