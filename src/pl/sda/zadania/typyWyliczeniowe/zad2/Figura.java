package pl.sda.zadania.typyWyliczeniowe.zad2;

public enum Figura {
    AS,
    WALET,
    KROLOWA,
    KROL,
    DWOJKA,
    TROJKA,
    CZWORKA,
    PIATKA,
    SZOSTKA,
    SIODEMKA,
    OSEMKA,
    DZIEWIATKA,
    DZIESIATKA;

}
