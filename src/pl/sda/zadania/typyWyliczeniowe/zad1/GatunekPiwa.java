package pl.sda.zadania.typyWyliczeniowe.zad1;

public enum GatunekPiwa {

    LAGER("Piwo Lager", "jasne"),
    PILZNER("Piwo Pilzner", "jasne"),
    STOUT("Piwo Stout", "ciemne"),
    PORTER("Piwo Porter", "ciemne"),
    MIODOWE("Piwo Miodowe", "jasne");

    private String nazwaPiwa;
    private String typPiwa;

    GatunekPiwa(String nazwaPiwa, String typPiwa) {

        this.nazwaPiwa = nazwaPiwa;
        this.typPiwa = typPiwa;

    }

    public String pobierzNazwePiwa() {
        return nazwaPiwa;
    }

    public String pobierzTypPiwa() {
        return typPiwa;
    }
}

